package com.example.task16

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import com.example.task16.databinding.FragmentDatePickerBinding
import java.lang.ClassCastException

class DatePickerFragment : DialogFragment() {
    lateinit var binding:FragmentDatePickerBinding
    public interface OnInputChanged{
        fun sendData(str:String)
    }

    public lateinit var mOnInputChanged:OnInputChanged
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDatePickerBinding.inflate(inflater)

        binding.editTextDate3.setText(CurrentDataAndTitle.tmpDate)
        binding.cancel.setOnClickListener {
            dismiss()
        }

        binding.apply.setOnClickListener {
            CurrentDataAndTitle.tmpDate = binding.editTextDate3.text.toString()
            mOnInputChanged.sendData(CurrentDataAndTitle.tmpDate)
            dismiss()
        }

        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            mOnInputChanged = targetFragment as DatePickerFragment.OnInputChanged
        }catch(e: ClassCastException){

        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = DatePickerFragment()
    }
}