package com.example.task16

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.example.task16.databinding.FragmentCrimeBinding
import com.example.task16.model.Crime

class CrimeFragment : Fragment(), TitlePicker.OnInputChanged, DatePickerFragment.OnInputChanged {

    val currentCrime:Crime = Crime("Преступление", 1, "2000.03.08", false)
   lateinit var binding: FragmentCrimeBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCrimeBinding.inflate(inflater)
        setValues()


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.chTitle.setOnClickListener {
            val customDialog = TitlePicker()
            customDialog.setTargetFragment(this, 1)
            CurrentDataAndTitle.tmpString = currentCrime.getTitle()
            fragmentManager?.let { it1 -> customDialog.show(it1, "customDialog") }

        }

        binding.chDate.setOnClickListener {
            val customDialog = DatePickerFragment()
            CurrentDataAndTitle.tmpDate = currentCrime.getData()
            customDialog.setTargetFragment(this, 1)
            fragmentManager?.let { it1 -> customDialog.show(it1, "customDialog") }
        }
    }

    fun setTitle(str:String){
        currentCrime.setTitle(str)
    }

    private fun setValues() {
        binding.date.text = currentCrime.getData()
        binding.title.text = currentCrime.getTitle()
        binding.issolved.text =currentCrime.getIsSolved()
    }

    companion object {
        @JvmStatic
        fun newInstance() = CrimeFragment

    }

    override fun sendTitle(str: String) {
        currentCrime.setTitle(CurrentDataAndTitle.tmpString)
        setValues()

    }

    override fun sendData(str: String) {
        currentCrime.setData(CurrentDataAndTitle.tmpDate)
        setValues()
    }
}