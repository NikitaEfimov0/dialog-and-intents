package com.example.task16.model

import java.util.*

class Crime(t:String, i:Int, d:String, iS:Boolean) {
    private var title:String = t
    private var id:Int = i
    private var date:String = d
    private var isSolved:Boolean = iS

    fun getData():String{
        return date
    }

    fun setData(d:String){
        date = d
    }

    fun getTitle():String{
        return title
    }

    fun setTitle(t:String){
        title = t
    }

    fun getIsSolved():String{
        if(isSolved)
            return "Закрыто"
        return "Открыто"
    }
}