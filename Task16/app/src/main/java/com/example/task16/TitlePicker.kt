package com.example.task16

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import com.example.task16.databinding.FragmentTitlePickerBinding
import java.lang.ClassCastException


class TitlePicker : DialogFragment() {
   lateinit var binding:FragmentTitlePickerBinding

   public interface OnInputChanged{
       fun sendTitle(str:String)
   }

    public lateinit var mOnInputChanged:OnInputChanged

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTitlePickerBinding.inflate(inflater)

        binding.editTextTime.setText(CurrentDataAndTitle.tmpString)

        binding.cancel2.setOnClickListener {
            dismiss()
        }

        binding.apply2.setOnClickListener {
            CurrentDataAndTitle.tmpString = binding.editTextTime.text.toString()
            Toast.makeText(requireContext(), CurrentDataAndTitle.tmpString, Toast.LENGTH_LONG).show()
            mOnInputChanged.sendTitle(CurrentDataAndTitle.tmpString)
            dismiss()
        }

        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
              mOnInputChanged = targetFragment as OnInputChanged
        }catch(e:ClassCastException){

        }
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) = TitlePicker()

    }
}