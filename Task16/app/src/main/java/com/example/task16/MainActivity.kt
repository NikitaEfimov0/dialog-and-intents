package com.example.task16

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.task16.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}